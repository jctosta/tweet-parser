// Dependências do Express
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var path = require("path");

// Logging
var logger = require('winston');
logger.add(logger.transports.File, { filename: 'app.log' });

var validator = require("validator");

// Envio de email
var Mailgun = require('mailgun').Mailgun;
var mg = new Mailgun('key-fdf9c5c8892abf35331b13aab69d94b0');

// Configuração do Express
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(error);

var port = process.env.PORT || 3000;

function error(err, req, res, next) {
    logger.error(err.stack);
    res.status(500).send({ error: err });
}

var router = express.Router();

router.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
})

router.post('/', function(req, res) {
    
    logger.info(req.body);
    
    // Texto do tweet
    var tweetText = req.body.text;
    if (tweetText.length === 0) {
        throw new Error('Invalid Tweet!!!');
    }
    // Sanitizando a expressão de busca    
    var searchExpression = validator.escape(req.body.search);
    searchExpression = validator.stripLow(searchExpression);
    if (searchExpression.length === 0) {
        throw new Error('Invalid Expression!!!');
    }    
    // Email de retorno
    var returnMail = req.body.mail;
    if (returnMail.length === 0) {
        throw new Error('Invalid Email!!!');
    }
    // Expressão para validar email
    if (!validator.isEmail(returnMail)) {
        throw new Error('Invalid Email!!!');
    }
    
    
    // Verifica o tweet em busca da expressão de busca
    if (tweetText.toLowerCase().match(searchExpression.toLowerCase())) {
        // Envia um email em caso de sucesso.
        mg.sendText('jctosta86@gmail.com', 
            returnMail,
            "Texto Encontrado!!!",
            "O texto '" + searchExpression + "' foi encontrado no Tweet: " + tweetText,
            function(err) {
                if (!err) {
                    logger.info("Message sent!!!");
                    res.json({ message: "Message sent!!!" });
                }
                err && logger.error(err);
            });
    } else {
        logger.info("Text not found!!!");
        res.json({ message: "Text not found!!!" });
    }
});

app.use("/", router);

app.listen(port);

logger.info("Magic happens on port " + port);