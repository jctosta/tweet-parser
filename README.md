# Tweet Parser

Web service para validação de tweets.

## Tecnologias

- NodeJS (Runtime)
- ExpressJS (Middleware)
- Winston (Log)
- Validator.js (Sanitização e Validação)
- PM2 (Controle e Inicialização)
- Mailgun (Envio de Emails)